const fs = require('fs');
const nanoid = require('nanoid');

const path = "./messages";
let data = [];

module.exports = {
  init: () => {
    let innerData = [];
    return  new Promise((resolve, reject) => {
      fs.readdir(path, (err, files) => {
        if (err) {
          reject(err);
        } else {
          files.forEach(file => {
            innerData.push(file);
          });

          data = innerData.slice(-5);
          resolve(data);
        }
      })
    });
  },
  getData: () => data,
  addMessage: (message) => {
    let datetime = new Date().toISOString();

    message.datetime = datetime;
    message.id = nanoid();
    data.push(message);

    let contents = JSON.stringify(data, null, 2);

    return new Promise((resolve, reject) => {
      fs.writeFile(path + '/' + datetime + '.txt', contents, err => {
        if (err) {
          reject(err);
        } else {
          resolve(message);
        }
      });
    });
  }
};