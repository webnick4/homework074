const express = require('express');
const fileDb = require('./fileDb');
const messages = require('./app/messages');

const app = express();
const port = 8000;

app.use(express.json());

fileDb.init().then(() => {
  app.use('/messages', messages(fileDb));

  app.listen(port, () => {
    console.log(`Server is running on ${port} port`);
  });
});
